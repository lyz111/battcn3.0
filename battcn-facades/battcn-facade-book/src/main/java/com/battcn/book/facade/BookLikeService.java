package com.battcn.book.facade;

import com.battcn.book.pojo.po.BookLike;
import com.battcn.framework.mybatis.service.BaseService;

/**
 * @author Levin
 * @since 2018/03/08
 */
public interface BookLikeService extends BaseService<BookLike>{
}
