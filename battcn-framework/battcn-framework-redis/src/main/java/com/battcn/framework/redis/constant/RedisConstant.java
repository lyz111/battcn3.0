package com.battcn.framework.redis.constant;

/**
 * <p>redis常量</p>
 *
 * @author Levin
 * @since 2018/03/20
 */
public final class RedisConstant {

    public final static String CACHE_SERVICE = "cacheService";
    public final static String CACHE_TEMPLATE_NAME = "redisCacheTemplate";
    public final static String SEQUENCE_TEMPLATE_NAME = "sequenceRedisTemplate";
    public final static String LOCK_TEMPLATE_NAME = "lockRedisTemplate";
    public final static String LIMIT_TEMPLATE_NAME = "limitRedisTemplate";
    public final static String SEQUENCE_GENERATOR = "sequenceGenerator";
    public final static String LOCK_KEY_GENERATOR = "lockKeyGenerator";
    public final static String LIMIT_KEY_GENERATOR = "limitKeyGenerator";
}
